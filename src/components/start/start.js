import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { withRouter } from 'react-router';
import Header from '../../components/header/header';
import Footer from '../../components/footer/footer';


class Start extends Component {
  constructor(props) {
    super(props);
    
    this.state = {};
    this._handleKeyPress = this._handleKeyPress.bind(this);
  }
  
  _handleKeyPress(e) {
    if (e.keyCode === 13) {
      this.props.history.push(`/login`);
    }
  }
  
  componentDidMount() {
    document.addEventListener("keyup", this._handleKeyPress);
  }
  
  componentWillUnmount() {
    document.removeEventListener("keyup", this._handleKeyPress);
  }

  render() {
    return(
      <div className="l-content" tabIndex="1">
        <Header />
        <div className="c-form">
          <div className="c-p-wrap">
            <p className="p">Opzoek naar jouw m<sup>2</sup> regenwoud?</p>
          </div>
          <NavLink className="o-button--navLink" to="/login" >
            <button className="o-button o-button--green">Start</button>
          </NavLink>
        </div>
        <Footer/>
      </div>
    );
  }
}

export default withRouter(Start);