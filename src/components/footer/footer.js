import React, {Component} from 'react';
import FTF_logo from '../../images/FTF_logo.png';

class Footer extends Component {
  
  render() {
    return(
      <div className="c-brand-wrap">
        <img src={FTF_logo} alt="IMG"/>
      </div>
    );
  }
}

export default Footer;