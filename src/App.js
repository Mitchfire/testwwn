import React, { Component } from 'react';
import { Route } from 'react-router';
import { BrowserRouter } from 'react-router-dom';
import './styles.scss';
import './styles.css';
import rawUsers from './user.json';
import Start from './components/start/start';
import Login from './components/login/login';
import Overview from './components/overview/overview';
import Map from './components/map/map';

class App extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      users: undefined,
      user: undefined,
      userLoaded: false
    }
    
    this.setUser = this.setUser.bind(this);
    this.getUserByEmail = this.getUserByEmail.bind(this);
    this.toggleAreaIsSelected = this.toggleAreaIsSelected.bind(this);
    this.checkEmail = this.checkEmail.bind(this);
    this.checkAreasSelected = this.checkAreasSelected.bind(this);
    this.setAllAreasSelected = this.setAllAreasSelected.bind(this);
  }  
    UserJSON() {
      let users = {};
      rawUsers.forEach((user) => {
        users[user.email] = user;
      })
      this.setState({users: users, userLoaded: true});
    }
    
    getUserByEmail(email) {
    if (this.state.users[email]) {    
      return this.state.users[email] 
    } else {
      return console.log("Niet het goede emailadres")
    }
  }
  
   checkEmail(email) { 
    if(!this.state.user) {
      if (this.state.userLoaded) {
        //zorg dat de url email gelijk is aan die van het user object
        let user = this.getUserByEmail(email);
         if (user) {
          this.setUser(user);
        } 
      }
    }
  }

  componentDidMount() {
    this.UserJSON();
  }
  
  setUser(user) {
    this.setState({ user: user})
  }
    
  toggleAreaIsSelected(key) {
    let user = this.state.user;
    user.areas[key].isSelected = user.areas[key].isSelected ? false : true;
    this.setState({ user: user}); 
  }
  
  setAllAreasSelected() {
    const user = this.state.user;
    for (let i = 0; i < user.areas.length; i++) {
      user.areas[i].isSelected = true;
    }
  }
  
  checkAreasSelected() {
    const user = this.state.user;
    for (let i = 0; i < user.areas.length; i++) {
      if (user.areas[i].isSelected === true) {
        return true; 
      }
    }
    return false;
  }
  
  render() {
     const propsLogin = {
      setUser: this.setUser,
      getUserByEmail: this.getUserByEmail
    }
     
     const propsOverview = {
       setUser: this.setUser,
       getUserByEmail: this.getUserByEmail,
       userLoaded : this.state.userLoaded,
       user: this.state.user,
       toggleAreaIsSelected: this.toggleAreaIsSelected,
       checkEmail : this.checkEmail
     }
     
     const propsMap = {
       setUser: this.setUser,
       getUserByEmail: this.getUserByEmail,
       userLoaded : this.state.userLoaded,
       user: this.state.user,
       checkEmail : this.checkEmail,
       checkAreasSelected : this.checkAreasSelected,
       setAllAreasSelected : this.setAllAreasSelected
     }
     
    return (
        <BrowserRouter>
          <div className="l-app">
            <Route exact path='/' component={Start} />
            <Route exact path='/login' render={(props) => <Login {...propsLogin} /> } />
            <Route exact path='/overview/:email' render={(props) => <Overview {...propsOverview} /> } />
            <Route exact path='/map/:email' render={(props) => <Map {...propsMap} /> } />
          </div>
        </BrowserRouter>    
    );
  }
}

export default App;
