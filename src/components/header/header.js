import React, { Component} from 'react';
import WWN_logo from '../../images/WWN_logo.png';

class Header extends Component {
  
  render() {
    return(
      <div className="c-logo__main">
        <img className="o-logo__main" src={WWN_logo} alt='Img' />
        <div className="c-h3-wrap">
          <h3 className="c-logo__text">Adopteer Regenwoud</h3>
        </div>
      </div>
    );
  }
}

export default Header;