import React, {Component} from 'react';
import { withRouter } from 'react-router';
import WWN_logo from '../../images/WWN_logo.png';
import Arrow from '../../images/Arrow.png';
import Footer from '../../components/footer/footer';

class Login extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      email : undefined,
      emailError: undefined
    }; 
    
    this._handleKeyPress = this._handleKeyPress.bind(this);
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }
  
  _handleKeyPress(e) {
    if (e.keyCode === 13) {
      this.handleSubmit();
    }
  }
  
  componentDidMount() {
    this.emailInput.focus();
    document.addEventListener("keyup", this._handleKeyPress);
  }
  
  componentWillUnmount() {
    document.removeEventListener("keyup", this._handleKeyPress);
  }
  
  handleChange(event) {
    this.setState({email : event.target.value })
  }
  
  handleBackButtonClick() {
    this.props.history.push('/');
  }

  handleSubmit() {
    let user = this.props.getUserByEmail(this.state.email);
    if (user) {
      this.props.setUser(user);
      this.props.history.push(`/overview/${this.state.email}`);
    } else {
      this.setState({emailError: 'Fout emailadres'});
      this.emailInput.focus();
    }
  }
    
  render() {
    return (
      <div className="l-content">
      <img className="o-button__arrow o-button__arrow--showMobile" src={Arrow} alt="Img" onClick={this.handleBackButtonClick} />
        <div className="c-logo__main">
        <img className="o-logo__main o-logo__main--hideMobile" src={WWN_logo} alt='Img' />
          <div className="c-h3-wrap">
            <h3 className="c-logo__text">Adopteer Regenwoud</h3>
          </div>
        </div>
        <div className="l-form c-formMail">
          <div className="c-p-wrap">
            <p className="p"> Wat is je <b>mail adres</b>?</p>
          </div>
        </div>
        <input ref={(input) => { this.emailInput = input;}} className="c-input c-input--lineHeightMobile" type="email" maxLength="50" onChange={(event) => this.handleChange(event)}/>
        <span className="c-emailError"> {this.state.emailError && this.state.emailError} </span><br />
        <button id="submit" className="o-button o-button--green" onClick={() => {this.handleSubmit()}}>Verder</button>
        <Footer/>
      </div> 
    );
  }
}

export default withRouter(Login);