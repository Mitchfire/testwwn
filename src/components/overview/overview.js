import React, {Component} from 'react';
import { NavLink } from 'react-router-dom';
import {withRouter} from 'react-router';
import add from '../../images/add.png';
import vink from '../../images/vink.png';
import WWN_logo from '../../images/WWN_logo.png';
import Arrow from '../../images/Arrow.png';
import Footer from '../../components/footer/footer';

class Overview extends Component {
  constructor(props) {
    super(props);
    
    this.state = {};
    
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.enterSubmit = this.enterSubmit.bind(this);
  }

  componentDidMount() {
   this.props.checkEmail(this.props.match.params.email);
   document.addEventListener("keyup", this.enterSubmit);
  }
  
  componentDidUpdate() {
    this.props.checkEmail(this.props.match.params.email);  
  }
  
  enterSubmit(e, mapUrl) {
     if (e.keyCode === 13) {
       console.log("yay gelukt");
      this.props.history.push(this.props.user ? `/map/${this.props.user.email}` : '');
    }
  }
  
  handleBackButtonClick() {
    this.props.history.push('/login');
  }
  
  handleClick(i) {
    this.props.toggleAreaIsSelected(i);
  }

  render() {
    const mapUrl = this.props.user ? `/map/${this.props.user.email}` : '';
    return (
        <div className="l-content">
        <img className="o-button__arrow o-button__arrow--showMobile" src={Arrow} alt="Img" onClick={this.handleBackButtonClick}/>
          <div className="c-logo__main c-logo__main--hideMobile">
            <img className="o-logo__main" src={WWN_logo} alt='Img' />
            <div className="c-h3-wrap">
              <h3 className="c-logo__text">Adopteer Regenwoud</h3>
            </div>
          </div>
          <div className="l-form c-formOverview">
            <div className="c-p-wrap c-p-wrap--overview">
              <p className="c-formOverview__title c-formOverview__title--hideMobile">Selecteer de vierkante meters die je wilt bekijken</p>
              <p className="c-formOverview__title c-formOverview__title--showMobile">Welk stukje regenwoud wil je zien?</p>
            </div>
            <ul>
            {
              this.props.user && this.props.user.areas.map((area, i) => {
                return (
              <li className="c-overviewList" key={i}>
                <div className="c-overviewList__records c-overviewList__records--showMobile">
                  <div className="c-overviewList__amount">
                    <h4 className="h4_number">{area.vierkanteMeter}</h4><h4>m<sup>2</sup></h4>
                  </div>

                  <div className="c-overviewList__longlat">
                    <p className="p_longlat">{area.latitude}", {area.longitude}"</p>
                  </div>
                </div>

                <div className="c-overviewList__button">
                  <img src={area.isSelected ? vink : add} alt="IMG" onClick={ () => this.handleClick(i) } />
                </div>
              </li>
                )
              })
            }
            </ul>
            <NavLink className="o-button--navLink" to={mapUrl}>
              <button className="o-button o-button--green">Toon</button>
            </NavLink>
          </div>
          <Footer/>
        </div>
    );
  }
}

export default withRouter(Overview);