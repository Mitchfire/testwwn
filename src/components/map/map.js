import React, { Component } from "react";
import ReactMapGL, { Popup, Marker, FlyToInterpolator } from "react-map-gl";
import FTF_logo from "../../images/FTF_logo.png";
import { withRouter } from "react-router";

class Map extends Component {
  constructor(props) {
    super(props);

    this.state = {
      viewport: {
        width: 1200,
        height: 598,
        latitude: "",
        longitude: "",
        zoom: 10,
        //mapStyle: 'https://h2787917.stratoserver.net/styles/ForgetTheFish/style.json',
        mapboxApiAccessToken:
          "pk.eyJ1IjoibWl0Y2hmaXJlIiwiYSI6ImNqamp4MXB5ZTExaWozd3Nid2FyMTVwbDMifQ.gYkxOW54Xl1rBUhNTWAmiQ"
      },
      userLocation: {
        latitude: undefined,
        longitude: undefined
      },
      showCoords: false,
      popup: undefined
    };

    this.getUserLocation = this.getUserLocation.bind(this);
    this.getM2Location = this.getM2Location.bind(this);
    this.toggleVisibilityCoords = this.toggleVisibilityCoords.bind(this);
    this.showPopupUser = this.showPopupUser.bind(this);
    this.showPopupM2 = this.showPopupM2.bind(this);
    this.closePopup = this.closePopup.bind(this);
    this.closePopupM2 = this.closePopupM2.bind(this);
  }

  componentDidMount() {
    this.getUserLocation();
    this.props.checkEmail(this.props.match.params.email);
  }

  componentDidUpdate() {
    this.props.checkEmail(this.props.match.params.email);
  }

  showPopupUser() {
    this.setState(
      {
        popup: "Dit is jouw Locatie"
      },
      function() {
        document.getElementById("popup").innerHTML = this.state.popup;
        document.getElementById("popup").style.display = "flex";
      }
    );
  }

  showPopupM2(markerInfo) {
    this.setState({
      popup: markerInfo
    });
  }

  closePopup() {
    document.getElementById("popup").style.display = "none";
  }

  closePopupM2(i) {
    document.getElementsByClassName("popupM2")[i].style.display = "none";
  }

  getUserLocation() {
    const success = o => {
      const viewport = {
        ...this.state.viewport,
        longitude: o.coords.longitude,
        latitude: o.coords.latitude,
        zoom: 12
      };
      const userLocation = {
        longitude: o.coords.longitude,
        latitude: o.coords.latitude
      };
      this.setState({
        viewport,
        userLocation
      });
    };

    const error = err => {
      console.warn(err);
    };

    navigator.geolocation.getCurrentPosition(success, error, {
      enableHighAccuracy: false,
      timeout: 30000
    });
  }

  toggleVisibilityCoords() {
    if (this.state.showCoords === false) {
      this.setState({
        showCoords: true
      });
    }
  }

  getM2Location() {
    const viewport = {
      ...this.state.viewport,
      longitude: parseInt(this.props.user.areas[0].longitude, 10),
      latitude: parseInt(this.props.user.areas[0].latitude, 10),
      zoom: 4,
      transitionDuration: 3000,
      transitionInterpolar: new FlyToInterpolator()
    };
    this.setState({
      viewport
    });
  }

  handlePointsClick() {
    if (!this.props.checkAreasSelected()) {
      this.props.setAllAreasSelected();
    }

    this.getM2Location();
    this.toggleVisibilityCoords();
  }

  handleLocationClick() {
    this.getUserLocation();
    if (this.state.showCoords === true) {
      this.setState({
        showCoords: false
      });
    }
  }

  _renderAreaMarker(area, index) {
    return (
      <Marker
        key={`marker-${index}`}
        longitude={city.longitude}
        latitude={city.latitude}
      >
        <CityPin size={20} onClick={() => this.setState({ popupInfo: city })} />
      </Marker>
    );
  }

  render() {
    return (
      <div className="l-content">
        <div className="c-button_wrapper">
          <button
            className="o-button o-button--show o-button--green"
            onClick={() => this.handleLocationClick()}
          >
            Ga naar mijn locatie
          </button>
          <button
            className="o-button o-button--show o-button--green"
            onClick={() => this.handlePointsClick()}
          >
            Ga naar mijn m<sup>2</sup>
          </button>
        </div>

        <ReactMapGL
          {...this.state.viewport}
          onViewportChange={viewport => this.setState({ viewport })}
        >
          {this.state.userLocation.latitude &&
            this.state.userLocation.longitude && (
              <Marker
                captureClick={true}
                captureDoubleClick={true}
                latitude={this.state.userLocation.latitude}
                longitude={this.state.userLocation.longitude}
              >
                <div
                  className="c-markerPoint"
                  onClick={() => this.showPopupUser()}
                />
                <div id="popup" onDoubleClick={() => this.closePopup()} />
              </Marker>
            )}
        </ReactMapGL>

        <div className="c-m2Form">
          <div className="c-m2Form__titleWrap">
            <div className="c-m2Form__title">
              <h3 className="c-title__text">Jouw vierkante meter:</h3>
              <h3 className="c-title__text--showMobile">
                Hier vind je jouw vierkante meter!
              </h3>
            </div>
          </div>
          <div className="c-m2Form__infoWrap">
            <div className="c-m2Form__info">
              <ul className="c-m2Form__info--ul">
                {this.props.user &&
                  this.props.user.areas.map((area, i) => {
                    return (
                      this.state.showCoords === true &&
                      this.props.user.areas[i].isSelected === true && (
                        <li className="c-mapList" key={i}>
                          {this.props.user.areas[i].latitude}
                          ", {this.props.user.areas[i].longitude}"
                        </li>
                      )
                    );
                  })}
              </ul>
            </div>
          </div>
        </div>

        <div className="c-brand-wrap c-brand-wrap--hideMobile">
          <img src={FTF_logo} alt="IMG" />
        </div>
      </div>
    );
  }
}

export default withRouter(Map);
